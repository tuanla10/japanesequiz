//
//  Common.h
//  AirFilm
//
//  Created by PhongNT18 on 1/6/14.
//  Copyright (c) 2014 Z-Team. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface Common : NSObject

+ (CGFloat) detectIOSVersion;

+ (BOOL)IS_IPHONE5;

+ (void)showAlert:(NSString *)message title:(NSString *)title;

+ (CGFloat)heightForViewWithText:(NSString *)text andFont:(UIFont *)font andSizeWidth:(CGFloat)width;

+ (BOOL)isNullObject:(id)rawObject;

+ (BOOL)isNullString:(NSString *)rawString;

+ (NSString *)safeString:(NSString *)rawString;

+ (void)registerKeyboardEventForDelegate:(id)delegate keyboardShown:(SEL)keyShown keyboardWillHide:(SEL)keyWillHide;

+ (NSString *)documentsPath:(NSString *)fileName;

+ (NSString *)getPresentDateTime;

+ (void)listAllFont;

+ (void)saveCookies;
+ (void)loadCookies;
+ (void)clearCookies;

@end
