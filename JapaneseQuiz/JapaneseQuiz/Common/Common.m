//
//  Common.m
//  AirFilm
//
//  Created by PhongNT18 on 1/6/14.
//  Copyright (c) 2014 Z-Team. All rights reserved.
//

#import "Common.h"

@implementation Common

+(CGFloat) detectIOSVersion {
    NSString *ver = [[UIDevice currentDevice] systemVersion];
    CGFloat ver_float = [ver floatValue];
    return  ver_float;
}

+ (BOOL)IS_IPHONE5
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        if (screenSize.height > 480.0f) {
            return YES;
        } else {
            return NO;
        }
    } else {
        return NO;
    }
}

+ (void)showAlert:(NSString *)message title:(NSString *)title
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

+ (CGFloat)heightForViewWithText:(NSString *)text andFont:(UIFont *)font andSizeWidth:(CGFloat)width
{
    if (!font) {
        font = [UIFont systemFontOfSize:14.0];
    }
    CGSize sizeToFit = [text sizeWithFont:font
                        constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)
                            lineBreakMode:NSLineBreakByWordWrapping];
    
    return sizeToFit.height;
}

+ (BOOL)isNullObject:(id)rawObject
{
    if (!rawObject || [rawObject isKindOfClass:[NSNull class]])
        return YES;
    
    return NO;
}

+ (BOOL)isNullString:(NSString *)rawString
{
    if ([[self class] isNullObject:rawString])
        return YES;
    else if (![rawString isKindOfClass:[NSString class]])
        return YES;
    else if ([rawString isEqualToString:@""])
        return YES;
    
    return NO;
}

+ (NSString *)safeString:(NSString *)rawString
{
    if ([[self class] isNullString:rawString])
        return @"";
    else
        return rawString;
}


+ (void)registerKeyboardEventForDelegate:(id)delegate keyboardShown:(SEL)keyShown keyboardWillHide:(SEL)keyWillHide
{
    [[NSNotificationCenter defaultCenter] addObserver:delegate
                                             selector:keyShown
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:delegate
                                             selector:keyWillHide
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

+(NSString *)documentsPath:(NSString *)fileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:fileName];
}

+(NSString *)getPresentDateTime{
    
    NSDateFormatter *dateTimeFormat = [[NSDateFormatter alloc] init];
    [dateTimeFormat setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    
    NSDate *now = [[NSDate alloc] init];
    
    NSString *theDateTime = [dateTimeFormat stringFromDate:now];
    
    dateTimeFormat = nil;
    now = nil;
    
    return theDateTime;
}

+ (void)listAllFont
{
    //DEBUG ALL FONTS
    for (NSString* family in [UIFont familyNames]) {
        ZLog(@"%@", family);
        
        for (NSString* name in [UIFont fontNamesForFamilyName: family]) {
            ZLog(@"    %@", name);
        }
    }
}


+ (void)saveCookies
{
    NSData *cookiesData = [NSKeyedArchiver archivedDataWithRootObject: [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject: cookiesData forKey: @"sessionCookies"];
    [defaults synchronize];
}

+ (void)loadCookies
{
    NSArray *cookies = [NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults] objectForKey: @"sessionCookies"]];
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    
    for (NSHTTPCookie *cookie in cookies){
        [cookieStorage setCookie: cookie];
    }
}

+ (void)clearCookies
{
    //Clear cookie to fake new user
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (NSHTTPCookie *cookie in [storage cookies]) {
        [storage deleteCookie:cookie];
    }
    
    //Also clear on disk
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"sessionCookies"];
    [defaults synchronize];
}
@end
