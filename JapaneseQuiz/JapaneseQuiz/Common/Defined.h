//
//  Defined.h
//  AirFilm
//
//  Created by PhongNT18 on 1/6/14.
//  Copyright (c) 2014 Z-Team. All rights reserved.
//


#define METERS_PER_MILE     1609.344

#define kFontEurostileLTStd             @"EurostileLTStd"
#define kFontEurostileLTStdEx2          @"EurostileLTStd-Ex2"
#define kFontEurostileLTStdDemi         @"EurostileLTStd-Demi"

#define kUserInfo                       @"USER_INFO"

