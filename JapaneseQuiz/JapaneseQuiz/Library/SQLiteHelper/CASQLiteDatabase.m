//
//  SQLiteDatabase.m
//  Word Game
//
//  Created by Khaipham on 7/16/13.
//  Copyright (c) 2013 World Game. All rights reserved.
//

#import "CASQLiteDatabase.h"
#import "Defined.h"

#define PATHDATABASE [NSHomeDirectory() stringByAppendingFormat:@"/Documents/"]

CASQLiteDatabase *staticSQLiteDB;

@implementation CASQLiteDatabase

+ (CASQLiteDatabase *)shared
{
    if (!staticSQLiteDB) {
        [CASQLiteDatabase copyDataBaseToDocument:@"hiragana"];
        [CASQLiteDatabase copyDataBaseToDocument:@"katakana"];
        staticSQLiteDB = [[CASQLiteDatabase alloc] init];
    }
    return staticSQLiteDB;
}

+ (void)freeStaticDatabase
{
    // Assign static database to create new database
    staticSQLiteDB = nil;
}

+ (void)copyDataBaseToDocument:(NSString *) dbName
{
    NSString *documentPath = [NSString stringWithFormat:@"%@%@.db", PATHDATABASE, dbName];
    NSLog(@"SQLitePath: %@", documentPath);
    NSString *bundlePath = [[NSBundle mainBundle] pathForResource:dbName ofType:@"db"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:documentPath]) {
        NSError *error;
        [fileManager copyItemAtPath:bundlePath toPath:documentPath error:&error];
        if (error) {
            NSLog(@"Cannot copy file: %@", error);
        } else {
            NSLog(@"Copied to: %@", documentPath);
        }
    }
}

- (id)init
{
    if (self = [super init]) {
        NSString *dbName = [[NSUserDefaults standardUserDefaults] objectForKey:kSelectedJapaneseType];
        NSString *documentPath = [NSString stringWithFormat:@"%@%@.db", PATHDATABASE, dbName];
        sqlite3 *dbConnection;
        if (sqlite3_open([documentPath UTF8String], &dbConnection) != SQLITE_OK) {
            NSLog(@"[SQLITE] Unable to open database!");
            return nil; // if it fails, return nil obj
        }
        database = dbConnection;
    }
    return self;
}

- (id)initWithPath:(NSString *)path
{
    if (self = [super init]) {
        NSString *documentPath = path;
        sqlite3 *dbConnection;
        if (sqlite3_open([documentPath UTF8String], &dbConnection) != SQLITE_OK) {
            NSLog(@"[SQLITE] Unable to open database!");
            return nil; // if it fails, return nil obj
        }
        database = dbConnection;
    }
    return self;
}

- (NSArray *)performQuery:(NSString *)query {
    sqlite3_stmt *statement = nil;
    const char *sql = [query UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"[SQLITE] Error when preparing query!");
    } else {
        NSMutableArray *result = [NSMutableArray array];
        while (sqlite3_step(statement) == SQLITE_ROW) {
            NSMutableDictionary *row = [NSMutableDictionary dictionary];
            for (int i=0; i<sqlite3_column_count(statement); i++) {
                int colType = sqlite3_column_type(statement, i);
                NSString *colName = [NSString stringWithUTF8String:sqlite3_column_name(statement, i)];
                id value;
                if (colType == SQLITE_TEXT) {
                    const unsigned char *col = sqlite3_column_text(statement, i);
//                    value = [NSString stringWithFormat:@"%s", col];
                    value = [NSString stringWithUTF8String:(char*)col];
                } else if (colType == SQLITE_INTEGER) {
                    int col = sqlite3_column_int(statement, i);
                    value = [NSNumber numberWithInt:col];
                } else if (colType == SQLITE_FLOAT) {
                    double col = sqlite3_column_double(statement, i);
                    value = [NSNumber numberWithDouble:col];
                } else if (colType == SQLITE_NULL) {
                    value = @"";
                } else {
                    NSLog(@"[SQLITE] UNKNOWN DATATYPE");
                }
                
//                [row addObject:value];
                [row setObject:value forKey:colName];
            }
            [result addObject:row];
        }
        return result;
    }
    return nil;
}

- (void)insertRow:(NSDictionary*)values tableName:(NSString*)tableName
{
    NSString *query = [NSString stringWithFormat:@"INSERT INTO %@ ", tableName];
    NSString *key = @"(";
    NSString *value = @"(";
    for (NSString *k in [values allKeys]) {
        key = [key stringByAppendingFormat:@"\"%@\",", k];
        value = [value stringByAppendingFormat:@"\"%@\",", values[k]];
    }
    key = [key substringToIndex:key.length - 1];
    value = [value substringToIndex:value.length - 1];
    key = [key stringByAppendingFormat:@")"];
    value = [value stringByAppendingFormat:@")"];
    query = [NSString stringWithFormat:@"%@%@ VALUES %@", query, key, value];
    [self performQuery:query];
}

- (NSArray*)selectRow:(NSArray*)cols condition:(NSDictionary*)conditions tableName:(NSString*)tableName orderBy:(NSString *)orderBy
{
    NSString *query = [NSString stringWithFormat:@"SELECT "];
    if (cols && cols.count > 0) {
        for (NSString *col in cols) {
            query = [query stringByAppendingFormat:@"%@,", col];
        }
        query = [query substringToIndex:query.length - 1];
    } else {
        query = [query stringByAppendingFormat:@" * "];
    }
    query = [query stringByAppendingFormat:@" FROM %@ ", tableName];
    if (conditions.count > 0) {
        query = [NSString stringWithFormat:@"%@ WHERE ", query];
        for (NSString *key in conditions) {
            query = [NSString stringWithFormat:@"%@ \"%@\"=\"%@\" and", query, key, conditions[key]];
        }
        query = [query substringToIndex:query.length - 4];
    }
    if (orderBy) {
        query = [query stringByAppendingFormat:@" %@",orderBy];
    }
    return [self performQuery:query];
}

- (NSArray *)selectRow:(NSArray *)cols condition:(NSDictionary *)conditions tableName:(NSString *)tableName
{
    return [self selectRow:cols condition:conditions tableName:tableName orderBy:nil];
}

- (NSArray *)selectAllCols:(NSDictionary *)conditions tableName:(NSString *)tableName
{
    return [self selectRow:nil condition:conditions tableName:tableName];
}

- (NSArray *)selectAllCols:(NSDictionary *)conditions tableName:(NSString *)tableName orDerBy:(NSString *)orderBy
{
    return [self selectRow:nil condition:conditions tableName:tableName orderBy:orderBy];
}

- (NSArray *)selectAllTableName:(NSString *)tableName
{
    return [self selectRow:nil condition:nil tableName:tableName];
}

- (void)updateRow:(NSDictionary*)values condition:(NSDictionary*)conditions tableName:(NSString*)tableName
{
    NSString *query = [NSString stringWithFormat:@"UPDATE %@ SET ", tableName];
    for (NSString *key in [values allKeys]) {
        query = [NSString stringWithFormat:@"%@ \"%@\" = \"%@\",", query, key, values[key]];
    }
    query = [query substringToIndex:query.length - 1];
    if (conditions.count > 0) {
        query = [NSString stringWithFormat:@"%@ WHERE ", query];
        for (NSString *key in conditions) {
            query = [NSString stringWithFormat:@"%@ \"%@\"=\"%@\" and", query, key, conditions[key]];
        }
        query = [query substringToIndex:query.length - 4];
    }
    [self performQuery:query];
}

- (void)deleteRow:(NSDictionary*)conditions tableName:(NSString*)tableName
{
    NSString *query = [NSString stringWithFormat:@"DELETE FROM %@ WHERE ", tableName];
    if (conditions.count > 0) {
        for (NSString *key in conditions) {
            query = [NSString stringWithFormat:@"%@ \"%@\"=\"%@\" and", query, key, conditions[key]];
        }
    }
    query = [query substringToIndex:query.length - 4];
    [self performQuery:query];
}

@end
