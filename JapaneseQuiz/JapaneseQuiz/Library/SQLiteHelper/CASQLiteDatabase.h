//
//  SQLiteDatabase.h
//  Word Game
//
//  Created by Khaipham on 7/16/13.
//  Copyright (c) 2013 World Game. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface CASQLiteDatabase : NSObject {
    sqlite3 *database;
}

+ (CASQLiteDatabase*)shared;
+ (void)freeStaticDatabase;

- (id)initWithPath:(NSString*)path;
- (NSArray *)performQuery:(NSString *)query;

- (NSArray*)selectRow:(NSArray*)cols condition:(NSDictionary*)conditions tableName:(NSString*)tableName;
- (NSArray*)selectRow:(NSArray*)cols condition:(NSDictionary*)conditions tableName:(NSString*)tableName orderBy:(NSString*)orderBy;
- (NSArray*)selectAllTableName:(NSString*)tableName;
- (NSArray*)selectAllCols:(NSDictionary*)conditions tableName:(NSString*)tableName;
- (NSArray*)selectAllCols:(NSDictionary*)conditions tableName:(NSString*)tableName orDerBy:(NSString*)orderBy;

- (void)insertRow:(NSDictionary*)values tableName:(NSString*)tableName;
- (void)updateRow:(NSDictionary*)values condition:(NSDictionary*)conditions tableName:(NSString*)tableName;
- (void)deleteRow:(NSDictionary*)conditions tableName:(NSString*)tableName;

@end
