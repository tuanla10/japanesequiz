//
//  Common.h
//  AirFilm
//
//  Created by PhongNT18 on 1/6/14.
//  Copyright (c) 2014 Z-Team. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Defined.h"


@interface Common : NSObject
{
}

/*!
 * Global Variables
 */

@property (assign, nonatomic) NSInteger GAME_LEVEL;
@property (assign, nonatomic) kGameMode GAME_MODE;
@property (strong, nonatomic) NSString *SELECTED_DATABASE;



/*!
 * Static method
 */

+ (instancetype)sharedInstance;

+ (CGFloat)detectIOSVersion;

+ (BOOL)IS_IPHONE5;

+ (void)showAlert:(NSString *)message title:(NSString *)title;

+ (CGFloat)heightForViewWithText:(NSString *)text andFont:(UIFont *)font andSizeWidth:(CGFloat)width;

+ (BOOL)isNullObject:(id)rawObject;

+ (BOOL)isNullString:(NSString *)rawString;

+ (NSString *)safeString:(NSString *)rawString;

+ (void)registerKeyboardEventForDelegate:(id)delegate keyboardShown:(SEL)keyShown keyboardWillHide:(SEL)keyWillHide;

+ (NSString *)documentsPath:(NSString *)fileName;

+ (NSString *)getPresentDateTime;

+ (void)listAllFont;

+ (void)saveCookies;
+ (void)loadCookies;
+ (void)clearCookies;

+ (void)setDefaultsValue:(id)value forKey:(NSString *)key;
+ (id)getDefaultsValueForKey:(NSString *)key;

@end
