//
//  Defined.h
//  AirFilm
//
//  Created by PhongNT18 on 1/6/14.
//  Copyright (c) 2014 Z-Team. All rights reserved.
//


#define METERS_PER_MILE     1609.344


// ===========================================================================================
// DATABASE
// ===========================================================================================

//Key
#define kSelectedJapaneseType   @"k_selected_japanese_type" //katakana or hiragana
#define DATABASE_KATAKANA       @"katakana"
#define DATABASE_HIRAGANA       @"hiragana"

//Table data
#define WordTable               @"words"
#define PhrasesTable            @"phrases"


typedef NS_ENUM(NSInteger, kGameMode) {
    kGameModePlaying = 0,   // Playing game
    kGameModePractice = 1   // Practice
};

