//
//  ZWord.m
//  JapaneseQuiz
//
//  Created by Le Anh Tuan on 10/6/14.
//  Copyright (c) 2014 Le Anh Tuan. All rights reserved.
//

#import "ZWord.h"
#import "CASQLiteDatabase.h"
#import "Defined.h"

@implementation ZWord

+ (NSArray *) getAllStamps {
    NSArray *arr = [[CASQLiteDatabase shared] selectAllTableName:WordTable];
    NSMutableArray *returnArr =[[NSMutableArray alloc] init];
    int i = 1;
    if (arr.count > 0) {
        for (NSDictionary *dict in arr) {
            ZWord *word = [[ZWord alloc] init];
            word.wordID = i;
            word.wordCharacter = dict[@"character"];
            word.wordSoundMark = dict[@"soundmark"];
            word.wordLevel = [dict[@"level"] integerValue];
            [returnArr addObject:word];
            i++;
        }
        return returnArr;
    }
    return nil;
}

+ (NSArray *)getAllQuestions
{
    NSArray *resultList = [[CASQLiteDatabase shared] selectAllTableName:WordTable];
    
    return resultList;
}

@end
