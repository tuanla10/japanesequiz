//
//  ZWord.h
//  JapaneseQuiz
//
//  Created by Le Anh Tuan on 10/6/14.
//  Copyright (c) 2014 Le Anh Tuan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZWord : NSObject

@property (assign, nonatomic) NSInteger wordID;
@property (strong, nonatomic) NSString *wordCharacter;
@property (strong, nonatomic) NSString *wordSoundMark;
@property (assign, nonatomic) NSInteger wordLevel;
@property (assign, nonatomic) BOOL isHiragana;

+ (NSArray *) getAllStamps;

/*!
 * Get all question from selected database
 *
 * @return Array question on dictionary format
 */
+ (NSArray *)getAllQuestions;

@end
