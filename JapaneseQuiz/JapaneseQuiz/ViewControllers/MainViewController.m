//
//  MainViewController.m
//  JapaneseQuiz
//
//  Created by Gio Viet on 10/7/14.
//  Copyright (c) 2014 Z-Team. All rights reserved.
//

#import "MainViewController.h"
#import "SelectTypeVC.h"

@interface MainViewController ()
{
}


@property (weak, nonatomic) IBOutlet UIButton *btnPractice;
@property (weak, nonatomic) IBOutlet UIButton *btnPlayingGame;

@end

@implementation MainViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self initComponents];
    
    [self registerReceiveNotification];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    ZLog(@"");
    
    [self removeReceiveNotfication];
}

#pragma mark - Init layout

- (void)initComponents
{
    // Hide navigation bar
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    // Make button round conner
    _btnPlayingGame.layer.masksToBounds = YES;
    _btnPlayingGame.layer.cornerRadius = 5.0f;
    
    _btnPractice.layer.masksToBounds = YES;
    _btnPractice.layer.cornerRadius = 5.0f;
    
    //
    
}

- (void)registerReceiveNotification
{
    // Do nothing
}

- (void)removeReceiveNotfication
{
    // Do nothing
}

#pragma mark - Bind Data

#pragma mark - Action

- (IBAction)btnPracticeTouched:(id)sender
{
    [Common sharedInstance].GAME_MODE = kGameModePractice;
    
    [self gotoPlayingGameScreen];
}

- (IBAction)btnPlayingGameTouched:(id)sender
{
    [Common sharedInstance].GAME_MODE = kGameModePlaying;
    
    [self gotoPlayingGameScreen];
}

- (void)gotoPlayingGameScreen
{
    SelectTypeVC *typeVC = [[SelectTypeVC alloc] init];
    [self.navigationController pushViewController:typeVC animated:YES];
}


#pragma mark - Receive Notification

#pragma mark - UIKeyboardEvent

#pragma mark - UIAlerView

@end
