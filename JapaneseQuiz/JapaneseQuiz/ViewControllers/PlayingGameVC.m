//
//  PlayingGameVC.m
//  JapaneseQuiz
//
//  Created by Gio Viet on 10/6/14.
//  Copyright (c) 2014 Z-Team. All rights reserved.
//

#import "PlayingGameVC.h"
#import "ZWord.h"
#import <AudioToolbox/AudioToolbox.h>
#import "ASPopUpView.h"
#import "ASProgressPopUpView.h"

static NSInteger PLAYING_GAME_TIME = 120; //second
static NSInteger PRACTICE_TIME = 60;


@interface PlayingGameVC () <ASProgressPopUpViewDataSource>
{
    NSInteger currentLevel;
    CGFloat currentTime;
    CGFloat maxPlayingTime;
    
    NSArray *allWords;
    NSArray *allPhrase;
    NSArray *allQuestion;
    
    ZWord *question;
    NSMutableArray *answerArray;
    
    NSInteger countCorrect;
    NSInteger countFail;
    
    BOOL isTapWordGame;             // Tap word / Tap pronunciation
    BOOL isLockingSelected;         // Lock touch answer wating next question
    
    NSTimer *gameTimer;
    NSInteger minisecond;
}

// Level info
@property (weak, nonatomic) IBOutlet UILabel *lblLevel;
@property (weak, nonatomic) IBOutlet UIButton *btnTapWord;
@property (weak, nonatomic) IBOutlet UIButton *btnTapPronunciation;

@property (strong, nonatomic) IBOutlet UIView *levelInfoView;


// Playing game
@property (weak, nonatomic) IBOutlet UILabel *lblQuestion;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnCountDown;
@property (weak, nonatomic) IBOutlet UIButton *btnStaticAnswer;
@property (weak, nonatomic) IBOutlet UIButton *btnLevel;

@property (weak, nonatomic) IBOutlet UIView *questionView;

@property (weak, nonatomic) IBOutlet ASProgressPopUpView *progressView;

//
@property (strong, nonatomic) IBOutlet UIView *controlLevelView;
@property (weak, nonatomic) IBOutlet UIView *controlBoundView;

@end

@implementation PlayingGameVC


- (instancetype)initWithLevel:(NSInteger)level wordArray:(NSArray *)wordArr
{
    self = [super init];
    
    if (self) {
        currentLevel = level;
        allWords = [wordArr copy];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self initComponents];
    
    [self registerReceiveNotification];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    ZLog(@"");
    
    if (gameTimer) {
        [gameTimer invalidate];
        gameTimer = nil;
    }
    
    [self removeReceiveNotfication];
}

#pragma mark - Init layout

- (void)initComponents
{
    // ========================================================================
    // LEVEL INFO UI
    // ========================================================================
    
    _btnTapWord.layer.masksToBounds = YES;
    _btnTapWord.layer.cornerRadius = 5.0f;
    
    _btnTapPronunciation.layer.masksToBounds = YES;
    _btnTapPronunciation.layer.cornerRadius = 5.0f;
    
    _lblLevel.layer.masksToBounds = YES;
    _lblLevel.layer.cornerRadius = _lblLevel.frame.size.width/2;
    _lblLevel.text = [NSString stringWithFormat:@"%ld", (long)currentLevel];
    
    // Add level info to current view
    [self.view addSubview:_levelInfoView];
    
    
    
    // ========================================================================
    // PLAYING GAME UI
    // ========================================================================
    
    // Corner button
    for (id object in _questionView.subviews) {
        if ([object isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)object;
            
            button.layer.masksToBounds = YES;
            button.layer.cornerRadius = button.frame.size.width/2;
        }
    }
    
    _btnLevel.layer.masksToBounds = YES;
    _btnLevel.layer.cornerRadius = _btnLevel.frame.size.width/2;
    [_btnLevel setTitle:[NSString stringWithFormat:@"%ld", currentLevel] forState:UIControlStateNormal];
    
    // Init default
    allQuestion = [NSArray array];
    answerArray = [[NSMutableArray alloc] init];
}

- (void)registerReceiveNotification
{
    // Do nothing
}

- (void)removeReceiveNotfication
{
    // Do nothing
}

#pragma mark - Bind Data

- (void)nextRandomQuestion
{
    // Clear last state
    question = nil;
    [answerArray removeAllObjects];
    
    // Random question
    NSInteger totalQuestion = allQuestion.count;
    
    if (totalQuestion == 0) {
        return;
    }
    
    while ([answerArray count] < 4) {
        NSInteger r = arc4random() % totalQuestion; // ADD 1 TO GET NUMBERS BETWEEN 1 AND M RATHER THAN 0 and M-1
        
        if (![answerArray containsObject:allQuestion[r]]) {
            [answerArray addObject:allQuestion[r]];
        }
    }
    
    // Get random question from answerArray
    NSInteger r = arc4random() % answerArray.count;
    
    question = answerArray[r];
    
    // Show new question & answer
    if (isTapWordGame) {
        // TAP WORD GAME
        
        _lblQuestion.text = question.wordSoundMark;
        _lblDescription.text = nil;
        
        for (int i=0; i<answerArray.count; i++) {
            ZWord *answer = answerArray[i];
            
            
            UIButton *btn = (UIButton *)[_questionView viewWithTag:(i+1)];
            if (btn && [btn isKindOfClass:[UIButton class]]) {
                [btn setBackgroundColor:[UIColor lightGrayColor]];
                
                [btn setTitle:answer.wordCharacter forState:UIControlStateNormal];
            }
        }
        
        // Play sound
        [self playSound:question.wordSoundMark];
    }
    else {
        // TAP PRONUNCIATION GAME
        
        _lblQuestion.text = question.wordCharacter;
        _lblDescription.text = nil;
        
        for (int i=0; i<answerArray.count; i++) {
            ZWord *answer = answerArray[i];
            
            UIButton *btn = (UIButton *)[_questionView viewWithTag:(i+1)];
            if (btn && [btn isKindOfClass:[UIButton class]]) {
                [btn setBackgroundColor:[UIColor lightGrayColor]];
                
                [btn setTitle:answer.wordSoundMark forState:UIControlStateNormal];
            }
        }
    }
    
    // Allow user select answer
    isLockingSelected = NO;
}


#pragma mark - Action

- (IBAction)btnAnswertTouched:(id)sender
{
    if (isLockingSelected) {
        return;
    }
    
    
    UIButton *button = (UIButton *)sender;
    NSInteger answerIndex = button.tag - 1;
    
    // Don't allow user select other button
    isLockingSelected = YES;
    
    // Check user selected answer
    BOOL isCorrect = NO;
    
    if (answerIndex>=0 && answerIndex<answerArray.count) {
        ZWord *selectedAnswer = answerArray[answerIndex];
        
        if ([selectedAnswer isEqual:question]) {
            // Correct
            isCorrect = YES;
            countCorrect++;
            
            // Increase time
            currentTime += 3;
            
            if (currentTime > maxPlayingTime) {
                currentTime = maxPlayingTime;
                minisecond = 0;
            }
            
            [_progressView setProgress:(currentTime/maxPlayingTime) animated:YES];
            
        } else {
            countFail++;
        }
    }
    
    // Response selected answert
    UIColor *backColor = isCorrect ? [UIColor greenColor] : [UIColor redColor];
    [button setBackgroundColor:backColor];
    
    // Update static
    NSString *staticAnswer = [NSString stringWithFormat:@"%ld/%ld", (long)countCorrect, (long)countFail];
    [_btnStaticAnswer setTitle:staticAnswer forState:UIControlStateNormal];
    
//    NSString *timeLeft = [NSString stringWithFormat:@"%ld", (long)currentTime];
//    [_btnCountDown setTitle:timeLeft forState:UIControlStateNormal];
    
    // Playing sound
    if (!isTapWordGame) {
        // TAP PRONUNCIATION GAME
        [self playSound:question.wordSoundMark];
    }
    
    
    // Call next question
    [self performSelector:@selector(nextRandomQuestion) withObject:nil afterDelay:1.0f];
}

- (IBAction)btnHomeTouched:(id)sender
{
    // Cancel all perform selector
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnWordTouched:(id)sender
{
    isTapWordGame = YES;
    
    // Hide & Start game
    [self startNewGame];
}

- (IBAction)btnPronunciationTouched:(id)sender
{
    isTapWordGame = NO;
    
    // Hide & Start game
    [self startNewGame];
}

- (void)startNewGame
{
    // SET DATA
    allQuestion = [allWords copy];
    
    
    // Set value for game
    minisecond = 0;
    currentTime = PRACTICE_TIME;
    maxPlayingTime = PRACTICE_TIME;
    
    if ([Common sharedInstance].GAME_MODE == kGameModePlaying) {
        currentTime = PLAYING_GAME_TIME;
        maxPlayingTime = PLAYING_GAME_TIME;
    }
    
    
    // Hide level info & start new game
    [_levelInfoView removeFromSuperview];
    
    _progressView.font = [UIFont fontWithName:@"Futura-CondensedExtraBold" size:20];
    _progressView.popUpViewAnimatedColors = @[[UIColor redColor], [UIColor orangeColor], [UIColor greenColor]];
    _progressView.popUpViewCornerRadius = 16.0;
    _progressView.dataSource = self;
    _progressView.progress = currentTime / maxPlayingTime;
    
    [_progressView showPopUpViewAnimated:YES];
    
    
    // Init first question
    [self nextRandomQuestion];
    
    // Start timer
    if (gameTimer) {
        [gameTimer invalidate];
        gameTimer = nil;
    }
    
    gameTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                 target:self
                                               selector:@selector(countDownTimer)
                                               userInfo:nil
                                                repeats:YES];
}

#pragma mark - Receive Notification

#pragma mark - UIKeyboardEvent

#pragma mark - UIAlerView

#pragma mark - Processing

- (void)countDownTimer
{
    minisecond++;
    
    if (minisecond >= 10) {
        minisecond = 0;
        currentTime--;
        
        if (currentTime < 0) {
            
            if (gameTimer) {
                [gameTimer invalidate];
                
                
                [self showLevelControl];
            }
            return;
        }
        
        [_progressView setProgress:(currentTime/maxPlayingTime) animated:YES];
    }
}

#pragma mark - Other method

- (void)playSound:(NSString *)soundName
{
    //NSString *soundPath = [NSString stringWithFormat:@"%@.mp3", soundName];
    SystemSoundID soundID;
    NSString *soundFile = [[NSBundle mainBundle] pathForResource:soundName ofType:@"mp3"];
    
    AudioServicesCreateSystemSoundID((__bridge  CFURLRef) [NSURL fileURLWithPath:soundFile], & soundID);
    AudioServicesPlaySystemSound(soundID);
}


#pragma mark - ASP

- (NSString *)progressView:(ASProgressPopUpView *)progressView stringForProgress:(float)progress
{
    return [NSString stringWithFormat:@"%.0f", currentTime];
}


#pragma mark - Control Level

- (void)showLevelControl
{
    _controlBoundView.layer.cornerRadius = 10.0f;
    _controlBoundView.layer.masksToBounds = YES;
    
    _controlLevelView.frame = self.view.frame;
    _controlLevelView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    
    [self.view addSubview:_controlLevelView];
}

- (IBAction)btnRestartLevelTouched:(id)sender
{
    [_controlLevelView removeFromSuperview];
    
    // Restart new game
    [self startNewGame];
}

- (IBAction)btnLevelListTouched:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
