//
//  PlayingGameVC.h
//  JapaneseQuiz
//
//  Created by Gio Viet on 10/6/14.
//  Copyright (c) 2014 Z-Team. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonViewController.h"

@interface PlayingGameVC : CommonViewController
{
}

- (instancetype)initWithLevel:(NSInteger)level wordArray:(NSArray *)wordArr;

@end
