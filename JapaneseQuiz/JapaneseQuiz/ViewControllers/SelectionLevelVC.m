//
//  SelectionLevelVC.m
//  JapaneseQuiz
//
//  Created by Gio Viet on 10/8/14.
//  Copyright (c) 2014 Z-Team. All rights reserved.
//

#import "SelectionLevelVC.h"
#import "PlayingGameVC.h"
#import "ZWord.h"
#import "CASQLiteDatabase.h"
#import "LevelCell.h"

static NSString *LEVEL_CELL_ID = @"LevelCell";

@interface SelectionLevelVC () <UITableViewDataSource, UITableViewDelegate>
{
    NSArray *allKeys;
    NSMutableDictionary *dataList;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SelectionLevelVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self initComponents];
    
    [self registerReceiveNotification];
    
    [self loadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Show navigation bar
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Show navigation bar
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    ZLog(@"");
    
    [self removeReceiveNotfication];
}

#pragma mark - Init layout

- (void)initComponents
{
    // Tableview
    _tableView.backgroundColor = [UIColor clearColor];
    
    if ([_tableView respondsToSelector:@selector(contentInset)]) {
        [_tableView setContentInset:UIEdgeInsetsZero];
    }
    if ([_tableView respondsToSelector:@selector(separatorInset)]) {
        [_tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    UINib *cellNib = [UINib nibWithNibName:LEVEL_CELL_ID bundle:nil];
    [_tableView registerNib:cellNib forCellReuseIdentifier:LEVEL_CELL_ID];
    
}

- (void)registerReceiveNotification
{
    // Do nothing
}

- (void)removeReceiveNotfication
{
    // Do nothing
}


#pragma mark - Bind Data

- (void)loadData
{
    allKeys = [[NSArray alloc] init];
    dataList = [[NSMutableDictionary alloc] init];
    
    NSArray *allQuestion = [ZWord getAllStamps];
    
    for (int i=0; i<allQuestion.count; i++) {
        ZWord *word = allQuestion[i];
        
        NSNumber *keyNumber = [NSNumber numberWithInteger:word.wordLevel];
        //NSString *keyString = [NSString stringWithFormat:@"%ld", (long)word.wordLevel];
        
        NSMutableArray *array = (NSMutableArray *)dataList[keyNumber];
        if (!array) {
            array = [[NSMutableArray alloc] init];
        }
        
        [array addObject:word];
        
        [dataList setObject:array forKey:keyNumber];
    }
    
    // Get keys
    allKeys = [dataList allKeys];
    
    allKeys = [allKeys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [obj1 compare:obj2];
    }];
    
    
    
    // Set source for tableview
    _tableView.dataSource = self;
    _tableView.delegate = self;
}

#pragma mark - Action



#pragma mark - Receive Notification

#pragma mark - UIKeyboardEvent

#pragma mark - UIAlerView


#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return allKeys.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LevelCell *cell = [tableView dequeueReusableCellWithIdentifier:LEVEL_CELL_ID];
    
    NSNumber *keyNumber = allKeys[indexPath.row];
    
    NSMutableArray *array = dataList[keyNumber];
    
    NSInteger count = MIN(5, array.count);  // Max char for each row
    
    for (int i=0; i<5; i++) {
        NSInteger tag = (10+i+1);
        UIView *view = (UIView *)[cell viewWithTag:tag];
        if (view && [view isKindOfClass:[UIView class]]) {
            
            if (i<count) {
                view.hidden = NO;
                
                ZWord *word = array[i];
                
                UIButton *button = (UIButton *)[view viewWithTag:1];
                if (button && [button isKindOfClass:[UIButton class]]) {
                    [button setTitle:word.wordCharacter forState:UIControlStateNormal];
                }
                
                UILabel *label2 = (UILabel *)[view viewWithTag:2];
                if (label2 && [label2 isKindOfClass:[UILabel class]]) {
                    label2.text = word.wordSoundMark;
                }
            } else {
                view.hidden = YES;
            }
        }
    }
    
    NSString *levelString = [NSString stringWithFormat:@"%ld", [keyNumber longValue]];
    [cell.btnLevel setTitle:levelString forState:UIControlStateNormal];
    [cell.btnLevel setImage:nil forState:UIControlStateNormal];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSNumber *keyNumber = allKeys[indexPath.row];
    NSMutableArray *array = dataList[keyNumber];
    
    if (array && array.count > 0) {
        PlayingGameVC *playVC = [[PlayingGameVC alloc] initWithLevel:[keyNumber integerValue] wordArray:array];
        [self.navigationController pushViewController:playVC animated:YES];
    } else {
        [Common showAlert:@"No question for this level!" title:@"Notice"];
    }
}


@end
