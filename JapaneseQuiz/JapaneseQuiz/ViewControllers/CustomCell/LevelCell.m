//
//  LevelCell.m
//  JapaneseQuiz
//
//  Created by Gio Viet on 10/8/14.
//  Copyright (c) 2014 Z-Team. All rights reserved.
//

#import "LevelCell.h"

@implementation LevelCell

- (void)awakeFromNib
{
    // Initialization code
    
    UIColor *grayColor = [UIColor colorWithRed:225/255.0 green:225/255.0 blue:225/255.0 alpha:1];
    UIImage *grayImage = [self createCircleImageWithBackgroundColor:grayColor];
    
    [_btnLevel setBackgroundImage:grayImage forState:UIControlStateNormal];
    
    UIColor *blueColor = [UIColor colorWithRed:0 green:125/255.0 blue:208/255.0 alpha:1];
    UIImage *blueImage = [self createCircleImageWithBackgroundColor:blueColor];

    // Corner all label
    for (int i=1; i<=5; i++) {
        UIView *view = (UIView *)[self viewWithTag:(10+i)];
        
        if (view && [view isKindOfClass:[UIView class]]) {
            
            UIButton *button = (UIButton *)[view viewWithTag:1];
            if (button && [button isKindOfClass:[UIButton class]]) {
                [button setBackgroundImage:blueImage forState:UIControlStateNormal];
            }
        }
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (UIImage *)createCircleImageWithBackgroundColor:(UIColor *)backColor
{
    UIImage *blueCircle = nil;
    
    CGRect circleRect = CGRectMake(0, 0, 40.0f, 40.0f);
    
    UIGraphicsBeginImageContextWithOptions(circleRect.size, NO, 0.0f);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSaveGState(ctx);
    
    CGContextSetFillColorWithColor(ctx, [backColor CGColor]);
    CGContextFillEllipseInRect(ctx, circleRect);
    
    CGContextRestoreGState(ctx);
    blueCircle = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    
    return blueCircle;
}


@end
