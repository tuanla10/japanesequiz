//
//  LevelCell.h
//  JapaneseQuiz
//
//  Created by Gio Viet on 10/8/14.
//  Copyright (c) 2014 Z-Team. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LevelCell : UITableViewCell
{
}

@property (weak) IBOutlet UIButton *btnLevel;

@end
