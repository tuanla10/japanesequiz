//
//  CommonViewController.h
//  FastBook
//
//  Created by Gio Viet on 9/24/14.
//  Copyright (c) 2014 Z-Team. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommonViewController : UIViewController
{
}

- (void)setGeneralBackground;

@end
