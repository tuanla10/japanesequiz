//
//  SelectTypeVC.m
//  JapaneseQuiz
//
//  Created by Gio Viet on 10/7/14.
//  Copyright (c) 2014 Z-Team. All rights reserved.
//

#import "SelectTypeVC.h"
#import "SelectionLevelVC.h"
#import "CASQLiteDatabase.h"


@interface SelectTypeVC ()
{
}

@property (weak, nonatomic) IBOutlet UIButton *btnKatakana;
@property (weak, nonatomic) IBOutlet UIButton *btnHiragana;
@property (weak, nonatomic) IBOutlet UILabel *lblDatabase;

@end

@implementation SelectTypeVC


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self initComponents];
    
    [self registerReceiveNotification];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Show navigation bar
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Show navigation bar
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    ZLog(@"");
    
    [self removeReceiveNotfication];
}

#pragma mark - Init layout

- (void)initComponents
{
    // Make button round conner
    _btnKatakana.layer.masksToBounds = YES;
    _btnKatakana.layer.cornerRadius = 5.0f;
    
    _btnHiragana.layer.masksToBounds = YES;
    _btnHiragana.layer.cornerRadius = 5.0f;
    
    // ...
    _lblDatabase.text = [Common sharedInstance].GAME_MODE == kGameModePlaying ? @"Playing" : @"Practice";
}

- (void)registerReceiveNotification
{
    // Do nothing
}

- (void)removeReceiveNotfication
{
    // Do nothing
}

#pragma mark - Bind Data

#pragma mark - Action

- (IBAction)btnHiraganaTouched:(id)sender
{
    [Common sharedInstance].SELECTED_DATABASE = DATABASE_HIRAGANA;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:@"hiragana" forKey:kSelectedJapaneseType];
    [defaults synchronize];
    
    [self gotoPlayingGameScreen];
}

- (IBAction)btnKatakanaTouched:(id)sender
{
    [Common sharedInstance].SELECTED_DATABASE = DATABASE_KATAKANA;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:@"katakana" forKey:kSelectedJapaneseType];
    [defaults synchronize];
    
    [self gotoPlayingGameScreen];
}

- (void)gotoPlayingGameScreen
{
    // Release current database to create new
    [CASQLiteDatabase freeStaticDatabase];
    
    // Show new game
    SelectionLevelVC *levelVC = [[SelectionLevelVC alloc] init];
    
    [self.navigationController pushViewController:levelVC animated:YES];
}


#pragma mark - Receive Notification

#pragma mark - UIKeyboardEvent

#pragma mark - UIAlerView

@end
