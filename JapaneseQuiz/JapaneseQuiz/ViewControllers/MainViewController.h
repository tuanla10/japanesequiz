//
//  MainViewController.h
//  JapaneseQuiz
//
//  Created by Gio Viet on 10/7/14.
//  Copyright (c) 2014 Z-Team. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonViewController.h"

@interface MainViewController : CommonViewController

@end
